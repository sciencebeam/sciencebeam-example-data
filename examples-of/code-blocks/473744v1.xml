<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE article PUBLIC "-//NLM//DTD JATS (Z39.96) Journal Archiving and Interchange DTD v1.2d1 20170631//EN" "JATS-archivearticle1.dtd">
<article xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" article-type="article" dtd-version="1.2d1" specific-use="production" xml:lang="en">
<front>
<journal-meta>
<journal-id journal-id-type="publisher-id">BIORXIV</journal-id>
<journal-title-group>
<journal-title>bioRxiv</journal-title>
<abbrev-journal-title abbrev-type="publisher">bioRxiv</abbrev-journal-title>
</journal-title-group>
<publisher>
<publisher-name>Cold Spring Harbor Laboratory</publisher-name>
</publisher>
</journal-meta>
<article-meta>
<article-id pub-id-type="doi">10.1101/473744</article-id>
<article-version>1.1</article-version>
<article-categories>
<subj-group subj-group-type="author-type">
<subject>Regular Article</subject>
</subj-group>
<subj-group subj-group-type="heading">
<subject>New Results</subject>
</subj-group>
<subj-group subj-group-type="hwp-journal-coll">
<subject>Bioinformatics</subject>
</subj-group>
</article-categories>
<title-group>
<article-title>VariantKey - A Reversible Numerical Representation of Human Genetic Variants</article-title>
</title-group>
<contrib-group>
<contrib contrib-type="author">
<name><surname>Asuni</surname><given-names>Nicola</given-names></name>
</contrib>
<aff id="a1"><institution>Genomics plc<sup>&#x002A;</sup> King Charles House, Park End Street Oxford</institution>, OX1 1JD, <country>UK</country> <email>nicola.asuni@genomicsplc.com</email></aff>
</contrib-group>
<author-notes>
<fn id="n1" fn-type="other"><label>&#x002A;</label><p><ext-link ext-link-type="uri" xlink:href="https://www.genomicsplc.com">https://www.genomicsplc.com</ext-link></p></fn>
</author-notes>
<pub-date pub-type="epub"><year>2018</year></pub-date>
<elocation-id>473744</elocation-id>
<history>
<date date-type="received">
<day>19</day>
<month>11</month>
<year>2018</year>
</date>
<date date-type="rev-recd">
<day>19</day>
<month>11</month>
<year>2018</year>
</date>
<date date-type="accepted">
<day>19</day>
<month>11</month>
<year>2018</year>
</date>
</history>
<permissions>
<copyright-statement>&#x00A9; 2018, Posted by Cold Spring Harbor Laboratory</copyright-statement>
<copyright-year>2018</copyright-year>
<license license-type="creative-commons" xlink:href="http://creativecommons.org/licenses/by/4.0/"><license-p>This pre-print is available under a Creative Commons License (Attribution 4.0 International), CC BY 4.0, as described at <ext-link ext-link-type="uri" xlink:href="http://creativecommons.org/licenses/by/4.0/">http://creativecommons.org/licenses/by/4.0/</ext-link></license-p></license>
</permissions>
<self-uri xlink:href="473744.pdf" content-type="pdf" xlink:role="full-text"/>
<abstract>
<title>A<sc>bstract</sc></title>
<p>Human genetic variants are usually represented by four values with variable length: chromosome, position, reference and alternate alleles. There is no guarantee that these components are represented in a consistent way across different data sources, and processing variant-based data can be inefficient because four different comparison operations are needed for each variant, three of which are string comparisons. Working with strings, in contrast to numbers, poses extra challenges on computer memory allocation and data-representation. Existing variant identifiers do not typically represent every possible variant we may be interested in, nor they are directly reversible. To overcome these limitations, <italic>VariantKey</italic>, a novel reversible numerical encoding schema for human genetic variants, is presented here alongside a multi-language open-source software implementation (<ext-link ext-link-type="uri" xlink:href="http://github.com/genomicspls/variantkey">http://github.com/genomicspls/variantkey</ext-link>). VariantKey represents variants as single 64 bit numeric entities, while preserving the ability to be searched and sorted by chromosome and position. The individual components of short variants can be directly read back from the VariantKey, while long variants are supported with a fast lookup table.</p>
</abstract>
<kwd-group kwd-group-type="author">
<title>Keywords</title>
<kwd>allele</kwd>
<kwd>alternate</kwd>
<kwd>bioinformatics</kwd>
<kwd>chromosome</kwd>
<kwd>dna</kwd>
<kwd>encoding</kwd>
<kwd>genetic</kwd>
<kwd>genomics</kwd>
<kwd>high-performance</kwd>
<kwd>hpc</kwd>
<kwd>human</kwd>
<kwd>mutation</kwd>
<kwd>normalization</kwd>
<kwd>position</kwd>
<kwd>reference</kwd>
<kwd>rsid</kwd>
<kwd>software</kwd>
<kwd>variant</kwd>
<kwd>variantkey</kwd>
</kwd-group>
<counts>
<page-count count="8"/>
</counts>
</article-meta>
</front>
<body>
<sec id="s1">
<label>1</label>
<title>Introduction</title>
<p>There is no standard guideline for consistent representation of genetic variants. For a given genome assembly, different classes of genetic variants are typically represented in the <italic>Variant Call Format</italic> (VCF) [<xref ref-type="bibr" rid="c1">1</xref>] as a combination of four components: chromosome name, base position, reference allele and alternate allele(s). The alternate field can contain comma-separated strings for multiallelic variants but they can be decomposed into biallelic ones.</p>
<p>Since there is no guarantee that the variant components are represented in a consistent way across different data sources, a normalization step is also required. Even with normalized variants, basic operations can be inefficient due to the necessity to perform four different comparison operations for each variant, three of which are string comparisons. Working with strings poses extra challenges on storage, memory allocation and data-representation due to their variable-length nature.</p>
<p>Existing variant identifiers don&#x2019;t provide a solution to this general problem because they are typically assigned, they require an expensive lookup operation to be retrieved or reversed and they don&#x2019;t represent every possible variant we may be interested in. In particular, the <italic>dbSNP</italic> reference identifier (rs#, &#x201C;rs ID&#x201D; or &#x201C;rs tag&#x201D;) [<xref ref-type="bibr" rid="c2">2</xref>] can&#x2019;t uniquely identify a variant in a given reference genome.</p>
<p>To overcome these limitations, VariantKey, a novel reversible numerical encoding schema for human genetic variants, is presented here. VariantKey represents variants as a single 64 bit numeric entity, while preserving the ability to be searched and sorted by chromosome and position. The individual components of short variants can be directly read back from the VariantKey, while long variants are supported with a fast lookup table.</p>
<p>A reference implementation of VariantKey and utility functions is provided as a multi-language software library (<ext-link ext-link-type="uri" xlink:href="http://github.com/genomicspls/variantkey">http://github.com/genomicspls/variantkey</ext-link>) released under the open source MIT license. For portability and performance, the core source code is written in header-only C programming language in a way that is also compatible with C&#x002B;&#x002B;. Full wrappers for Go, Python, R and a basic implementation in Javascript are also provided. The source code is documented and fully covered by unit tests.</p>
</sec>
<sec id="s2">
<label>2</label>
<title>Human Genetic Variant Definition</title>
<p>A genetic variant is a difference from the reference DNA nucleotide sequence.</p>
<p>In this context, the human genetic variant for a given genome assembly is defined as the set of four components compatible with the VCF format:</p>
<list list-type="bullet">
<list-item>
<p><bold>CHROM</bold> - <italic>chromosome</italic>: An identifier from the reference genome. It only has 26 valid values: autosomes from 1 to 22, the sex chromosomes X&#x003D;23 and Y&#x003D;24, mitochondria MT&#x003D;25 and a symbol NA&#x003D;0 to indicate an invalid value.</p>
</list-item>
<list-item>
<p><bold>POS</bold> - <italic>position</italic>: The reference position in the chromosome, with the first base having position 0. The largest expected value is less than 250 million [<xref ref-type="bibr" rid="c3">3</xref>] to represent the last base pair in Chromosome 1.</p>
</list-item>
<list-item>
<p><bold>REF</bold> - <italic>reference base(s)</italic>: String containing a sequence of reference nucleotide letters. The value in the POS field refers to the position of the first base in the String.</p>
</list-item>
<list-item>
<p><bold>ALT</bold> - <italic>alternate base(s)</italic>: Single alternate non-reference allele. String containing a sequence of nucleotide letters. Multiallelic variants must be decomposed in individual biallelic variants.</p>
</list-item>
</list>
</sec>
<sec id="s3">
<label>3</label>
<title>Variant Decomposition and Normalization</title>
<sec id="s3a">
<label>3.1</label>
<title>Decomposition</title>
<p>In the Variant Call Format (VCF) the alternate field can contain comma-separated strings for multiallelic variants, while in this context we only consider biallelic variants to allow for allelic comparisons between different data sets.</p>
<p>For example, the multiallelic variant:
<preformat preformat-type="dialog">
{CHROM&#x003D;1, POS&#x003D;3759889, REF&#x003D;TA, ALT&#x003D;TAA, TAAA, T}
</preformat>
can be decomposed as three biallelic variants:
<preformat preformat-type="dialog">
{CHROM&#x003D;1, POS&#x003D;3759889, REF&#x003D;TA, ALT&#x003D;TAA}
{CHROM&#x003D;1, POS&#x003D;3759889, REF&#x003D;TA, ALT&#x003D;TAAA}
{CHROM&#x003D;1, POS&#x003D;3759889, REF&#x003D;TA, ALT&#x003D;T}
</preformat></p>
<p>In VCF files the decomposition from multiallelic to biallelic variants can be performed using the vt software tool [<xref ref-type="bibr" rid="c4">4</xref>] with the command:</p>
<preformat preformat-type="dialog">
vt decompose -s source.vcf -o decomposed.vcf
</preformat>
<p>The &#x201C;-s&#x201D; option (smart decomposition) splits up INFO and GENOTYPE fields that have number counts of R and A appropriately [<xref ref-type="bibr" rid="c5">5</xref>].</p>
</sec>
<sec id="s3b">
<label>3.2</label>
<title>Normalization</title>
<p>A normalization step is required to ensure a consistent and unambiguous representation of variants. As shown in <xref rid="fig1" ref-type="fig">Figure 1</xref>, there are multiple ways to represent the same variant, but only one can be considered &#x201C;normalized&#x201D; as defined by [<xref ref-type="bibr" rid="c6">6</xref>]:</p>
<fig id="fig1" position="float" fig-type="figure">
<label>Figure 1:</label>
<caption><p>Example of entries representing the same variant.</p></caption>
<graphic xlink:href="473744_fig1.tif"/>
</fig>
<list list-type="bullet">
<list-item>
<p>A variant representation is normalized if and only if it is left aligned and parsimonious.</p>
</list-item>
<list-item>
<p>A variant representation is left aligned if and only if its base position is smallest among all potential representations having the same allele length and representing the same variant.</p>
</list-item>
<list-item>
<p>A variant representation is parsimonious if and only if the entry has the shortest allele length among all VCF entries representing the same variant.</p>
</list-item>
</list>
<p>In VCF files the variant normalization can be performed using the vt software tool [<xref ref-type="bibr" rid="c7">7</xref>]:</p>
<preformat preformat-type="dialog">
vt normalize -m -r genome.fa -o normalized.vcf decomposed.vcf
</preformat>
<p>or the bcftools command [<xref ref-type="bibr" rid="c8">8</xref>]:</p>
<preformat preformat-type="dialog">
bcftools norm -f genome.fa -o normalized.vcf decomposed.vcf
</preformat>
<p>or decompose and normalize with a single command:</p>
<preformat preformat-type="dialog">
bcftools norm --multiallelics -any -f genome.fa -o normalized.vcf source.vcf
</preformat>
<sec id="s3b1">
<label>3.2.1</label>
<title>Normalization Function</title>
<p>Individual biallelic variants can be normalized using the <monospace>Normalize_Variant</monospace> function provided by the <italic>variantkey</italic> library. The <monospace>normalize_variant</monospace> function first checks if the reference allele matches the genome reference. The match is considered valid and consistent if there is a perfect letter-by-letter match, and valid but not consistent if one or more letter matches an equivalent one. Equivalent nucleotide letters are defined in <xref rid="tbl1" ref-type="table">Table 1</xref>.</p>
<table-wrap id="tbl1" orientation="portrait" position="float">
<label>Table 1:</label>
<caption><p>Single-letter nucleotide codes [<xref ref-type="bibr" rid="c9">9</xref>]</p></caption>
<graphic xlink:href="473744_tbl1.tif"/>
</table-wrap>
<p>If the reference allele is not valid, the <monospace>normalize_variant</monospace> function tries to find a reference match with one of the following variant transformations:</p>
<list list-type="bullet">
<list-item>
<p>swap the reference and alternate alleles - <italic>sometimes it is not clear which one is the reference and which one is the alternate allele</italic>.</p>
</list-item>
<list-item>
<p>flip the alleles letters (use the complement letters) - <italic>sometimes the alleles refers to the other DNA strand</italic>.</p>
</list-item>
<list-item>
<p>swap and flip.</p>
</list-item>
</list>
<p>Note that the <italic>swap</italic> and <italic>flip</italic> processes can lead to false positive cases, especially when considering <italic>Single Nucleotide Polymorphisms</italic> (SNP). The return code of the <monospace>normalize_variant</monospace> function can be used to discriminate or discard variants that are not consistent.</p>
<p>If the variant doesn&#x2019;t match the genome reference, then the original variant is returned with an error code.</p>
<p>If both alleles have length 1, the normalization is complete and the variant is returned. Otherwise, a custom implementation of the vt normalization algorithm [<xref ref-type="bibr" rid="c6">6</xref>] is applied:</p>
<list list-type="bullet">
<list-item>
<p>while break, do</p>
<list list-type="simple">
<list-item>
<p>- if any of the alelles is empty and the position is greater than zero, then</p>
<p>&#x002A; extend both alleles one letter to the left using the nucleotide in the corresponding genome reference position;</p>
</list-item>
<list-item>
<p>- else</p>
<p>&#x002A; if both alleles end with the same letter and they have length 2 or more, then</p>
<p>&#x2022; truncate the rightmost letter of each allele;</p>
<p>&#x002A; else</p>
<p>&#x2022; break (exit the while loop);</p>
</list-item>
</list>
</list-item>
<list-item>
<p>while both alleles start with the same letter and have length 2 or more, do</p>
<list list-type="simple">
<list-item>
<p>- truncate leftmost letter of each allele;</p>
</list-item>
</list>
</list-item>
</list>
<p>The genome reference binary file (fasta.bin), used by the <monospace>normalize_variant</monospace> function, can be obtained from a FASTA file using the <monospace>resources/tools/fastabin.sh</monospace> script. This script extracts only the first 25 sequences for chromosomes 1 to 22, X, Y and MT. Precompiled versions can be downloaded from:(<ext-link ext-link-type="uri" xlink:href="https://sourceforge.net/projects/variantkey/files">https://sourceforge.net/projects/variantkey/files</ext-link>)</p>
</sec>
<sec id="s3b2">
<label>3.2.2</label>
<title>Normalized VariantKey</title>
<p>The variantkey library provides the <monospace>normalized_variantkey</monospace> function that returns the VariantKey of the normalized variant. This function should be used instead of the <monospace>variantkey</monospace> function if the input variant is not normalized.</p>
</sec>
</sec>
</sec>
<sec id="s4">
<label>4</label>
<title>VariantKey Format</title>
<p>For a given reference genome the VariantKey format encodes a Human Genetic Variant (CHROM, POS, REF and ALT) as 64 bit unsigned integer number (8 bytes or 16 hexadecimal symbols).</p>
<p>If the variant has not more than 11 bases between REF and ALT, the correspondent VariantKey can be directly reversed to get back the individual CHROM, POS, REF and ALT components.</p>
<p>If the variant has more than 11 bases, or non-base nucleotide letters are contained in REF or ALT, the VariantKey can be fully reversed with the support of a binary lookup table.</p>
<p>The VariantKey format doesn&#x2019;t represent universal codes, it only encodes CHROM, POS, REF and ALT, so each code is unique for a given reference genome. The direct comparisons of two VariantKeys makes sense only if they both refer to the same genome reference.</p>
<table-wrap id="tbl2" orientation="portrait" position="float">
<label>Table 2:</label>
<caption><p>Example of VariantKey encoding</p></caption>
<graphic xlink:href="473744_tbl2.tif"/>
</table-wrap>
<p>The VariantKey is composed of 3 sections arranged in 64 bit:</p>
<sec id="s4a">
<label>4.1</label>
<title>CHROM Encoding</title>
<p>The chromosome is encoded as unsigned integer number: 1 to 22, X&#x003D;23, Y&#x003D;24, MT&#x003D;25, NA&#x003D;0. This section is 5 bit long, so it can store up to 2<sup>5</sup> &#x003D; 32 symbols, enough to contain the required 25 canonical chromosome symbols &#x002B; NA. The largest value is: 25 dec &#x003D; 19 hex &#x003D; 11001 bin. Values from 26 to 31 are currently reserved. They can be used to indicate 6 alternative modes to interpret the remaining 59 bit. For instance, one of these values can be used to indicate the encoding of variants that occurs in non-canonical contigs.</p>
<fig id="fig2" position="float" fig-type="figure">
<label>Figure 2:</label>
<caption><p>CHROM binary mask (F800000000000000 hex &#x003D; 17870283321406128128 dec)</p></caption>
<graphic xlink:href="473744_fig2.tif"/>
</fig>
<p>Example: <monospace>&#x2019;chr19&#x2019; str &#x003D; 19 dec &#x003D; 10011 bin</monospace></p>
</sec>
<sec id="s4b">
<label>4.2</label>
<title>POS Encoding</title>
<p>The reference position in the chromosome is encoded as unsigned integer number with the first base having position 0. This section is 28 bit long, so it can store up to 2<sup>28</sup> &#x003D; 268, 435, 456 symbols, enough to contain the maximum position found in the largest human chromosome.</p>
<fig id="fig3" position="float" fig-type="figure">
<label>Figure 3:</label>
<caption><title>POS binary mask (7FFFFFF80000000 hex &#x003D; 576460750155939840 dec)</title></caption>
<graphic xlink:href="473744_fig3.tif"/>
</fig>
<p>Example: <monospace>29238771 dec &#x003D; 0001101111100010010111110011 bin</monospace></p>
</sec>
<sec id="s4c">
<label>4.3</label>
<title>REF&#x002B;ALT Encoding</title>
<p>The reference and alternate alleles are encoded in 31 bit.</p>
<p>This section allow two different type of encodings:</p>
<sec id="s4c1">
<label>4.3.1</label>
<title>Non-reversible encoding</title>
<p>If the total number of nucleotides between REF and ALT is more then 11, or if any of the alleles contains nucleotide letters other than base A, C, G and T, then the LSB (least significant bit) is set to 1 and the remaining 30 bit are filled with an hash value of the REF and ALT strings.</p>
<fig id="fig4" position="float" fig-type="figure">
<label>Figure 4:</label>
<caption><p>REF&#x002B;ALT binary mask (7FFFFFFF hex &#x003D; 2147483647 dec)</p></caption>
<graphic xlink:href="473744_fig4.tif"/>
</fig>
<p>The hash value is calulated using a custom fast non-cryptographic algorithm based on <italic>MurmurHash3</italic> [<xref ref-type="bibr" rid="c10">10</xref>]. A lookup table (nrvk.bin) is required to reverse the REF and ALT values.</p>
<p>In the normalized dbSNP VCF file GRCh37.p13.b150 there are only 0.365&#x0025; (1,229,769 / 337,162,128) variants that requires this encoding. Amongst those, the maximum number of variants that share the same chromosome and position is 15. With 30 bit the probability of hash collision is approximately 10<sup>&#x2212;7</sup> for 15 elements, 10<sup>&#x2212;6</sup> for 46 and 10<sup>&#x2212;5</sup> for 146. The size of the non-reversible lookup table for dbSNP GRCh37.p13.b150 is only 45.7MB, so it can be easily contained in memory.</p>
</sec>
<sec id="s4c2">
<label>4.3.2</label>
<title>Reversible encoding</title>
<p>If the total number of nucleotides between REF and ALT is 11 or less, and they only contain base letters A, C, G and T, then the LSB is set to 0 and the remaining 30 bit are used as follows:</p>
<list list-type="bullet">
<list-item>
<p>bit 1-4 indicate the number of bases in REF; the capacity of this section is 2<sup>4</sup> &#x003D; 16 but the maximum expected value is 10 dec &#x003D; 1010 bin;</p>
</list-item>
<list-item>
<p>bit 5-8 indicate the number of bases in ALT; the capacity of this section is 2<sup>4</sup> &#x003D; 16 but the maximum expected value is 10 dec &#x003D; 1010 bin;</p>
</list-item>
<list-item>
<p>the following 11 groups of 2 bit are used to represent <monospace>REF</monospace> bases followed by <monospace>ALT</monospace>, with the following encoding:
<preformat preformat-type="dialog">
&#x2014; A &#x003D; 0 dec &#x003D; 00 bin
&#x2014; C &#x003D; 1 dec &#x003D; 01 bin
&#x2014; G &#x003D; 2 dec &#x003D; 10 bin
&#x2014; T &#x003D; 4 dec &#x003D; 11 bin
</preformat></p>
<p>Examples:
<fig id="ufig1" position="float" fig-type="figure">
<graphic xlink:href="473744_ufig1.tif"/>
</fig></p>
<p>The reversible encoding covers 99.635&#x0025; of the variants in the normalized dbSNP VCF file GRCh37.p13.b150.</p>
</list-item>
</list>
</sec>
</sec>
</sec>
<sec id="s5">
<label>5</label>
<title>VariantKey Properties</title>
<list list-type="bullet">
<list-item>
<p>It can be encoded and decoded on-the-fly.</p>
</list-item>
<list-item>
<p>Sorting by VariantKey is equivalent of sorting by CHROM and POS.</p>
</list-item>
<list-item>
<p>The 64 bit VariantKey can be exported as a single 16 character hexadecimal string.</p>
</list-item>
<list-item>
<p>Sorting the hexadecimal representation of VariantKey in alphabetical order is equivalent of sorting the VariantKey numerically.</p>
</list-item>
<list-item>
<p>Comparing two variants by VariantKey only requires comparing two 64 bit numbers, a very well optimized operation in current computer architectures. In contrast, comparing two normalized variants in VCF format requires comparing one numbers and three strings.</p>
</list-item>
<list-item>
<p>VariantKey can be used as a main database key to index data by "variant". This simplify common searching, merging and filtering operations.</p>
</list-item>
<list-item>
<p>All types of database joins between two data sets (inner, left, right and full) can be easily performed using the VariantKey as index.</p>
</list-item>
<list-item>
<p>When CHROM, REF and ALT are the only strings in a table, replacing them with VariantKey allows to work with numeric only tables with obvious advantages. This also allows to represent the data in a compact binary format where each column uses a fixed number of bit or bytes, with the ability to perform a quick binary search on the first sorted column.</p>
</list-item>
</list>
</sec>
<sec id="s6">
<label>6</label>
<title>Example Application</title>
<p>A direct application of the VariantKey representation is the ability to be used in binary lookup table files.</p>
<p>The variantkey library provides tools to create and use binary lookup table files to convert rsID to VariantKey and vice-versa:</p>
<list list-type="bullet">
<list-item>
<p><monospace>rsvk.bin</monospace></p>
<p>Lookup table to retrieve VariantKey from rsID.</p>
<p>This binary file can be generated by the <monospace>resources/tools/rsvk.sh</monospace> script from a TSV file.</p>
<p>This can also be in <italic>Apache Arrow</italic> file format with a single <italic>RecordBatch</italic>, or <italic>Feather</italic> format. The first column must contain the rsID sorted in ascending order.</p>
</list-item>
<list-item>
<p><monospace>vkrs.bin</monospace></p>
<p>Lookup table to retrieve rsID from VariantKey.</p>
<p>This binary file can be generated by the <monospace>resources/tools/vkrs.sh</monospace> script from a TSV file.</p>
<p>This can also be in <italic>Apache Arrow</italic> file format with a single <italic>RecordBatch</italic>, or <italic>Feather</italic> format. The first column must contain the VariantKey sorted in ascending order.</p>
</list-item>
</list>
<p>Software benchmarks on the variantkey library, performed using a laptop with 16GB of RAM and Intel core i7 7th gen CPU, shows that it is possible to retrieve from a file with 400M items, a VariantKey from rsID or viceversa in about 48ns, or more than 20M items per second.</p>
</sec>
<sec id="s7">
<label>7</label>
<title>Conclusions</title>
<p>Existing human genetic variant representation formats are not designed to maximize computational performance and do not provide a general solution to efficiently represent any arbitrary variant. VariantKey, a new reversible numerical encoding schema for human genetic variants is proposed here, with a publicly available implementation. This new computer-friendly format is expected to improve the performance of large-scale computation of variant-based genetic data.</p>
</sec>
</body>
<back>
<ref-list>
<title>References</title>
<ref id="c1"><label>[1]</label><mixed-citation publication-type="journal"><string-name><surname>Petr</surname> <given-names>Danecek</given-names></string-name>, <string-name><surname>Adam</surname> <given-names>Auton</given-names></string-name>, <string-name><surname>Goncalo</surname> <given-names>Abecasis</given-names></string-name>, <string-name><surname>Cornelis A.</surname> <given-names>Albers</given-names></string-name>, <string-name><surname>Eric</surname> <given-names>Banks</given-names></string-name>, <string-name><given-names>Mark A.</given-names> <surname>DePristo</surname></string-name>, <string-name><surname>Robert</surname> <given-names>E. Handsaker</given-names></string-name>, <string-name><surname>Gerton</surname> <given-names>Lunter</given-names></string-name>, <string-name><surname>Gabor</surname> <given-names>T. Marth</given-names></string-name>, <string-name><surname>Stephen</surname> <given-names>T. Sherry</given-names></string-name>, <string-name><surname>Gilean</surname> <given-names>McVean</given-names></string-name>, <string-name><surname>Richard</surname> <given-names>Durbin</given-names></string-name>, and <article-title>1000 Genomes Project Analysis Group. The variant call format and vcftools</article-title>. <source>Bioinformatics</source>, <volume>27</volume>(<issue>15</issue>):<fpage>2156</fpage>&#x2013;<lpage>2158</lpage>, <year>2011</year>.</mixed-citation></ref>
<ref id="c2"><label>[2]</label><mixed-citation publication-type="journal"><string-name><surname>Adrienne</surname> <given-names>Kitts</given-names></string-name>, <string-name><surname>Lon</surname> <given-names>Phan</given-names></string-name>, <string-name><surname>Minghong</surname> <given-names>Ward</given-names></string-name>, and <string-name><given-names>J.</given-names> <surname>Bradley Holmes</surname></string-name>. <article-title>The database of short genetic variation (dbsnp)</article-title>. <source>The NCBI Handbook</source>, Apr <year>2014</year>.</mixed-citation></ref>
<ref id="c3"><label>[3]</label><mixed-citation publication-type="other"><collab>Human genome assembly grch38.p12 -genome reference consortium</collab>. <ext-link ext-link-type="uri" xlink:href="https://www.ncbi.nlm.nih.gov/grc/human/data">https://www.ncbi.nlm.nih.gov/grc/human/data</ext-link>, Dec <year>2017</year>.</mixed-citation></ref>
<ref id="c4"><label>[4]</label><mixed-citation publication-type="journal"><string-name><surname>Adrian</surname> <given-names>Tan</given-names></string-name>. <source>Vt decompose</source>. <ext-link ext-link-type="uri" xlink:href="https://genome.sph.umich.edu/wiki/Vt#Decompose">https://genome.sph.umich.edu/wiki/Vt#Decompose</ext-link>, Mar <year>2013</year>.</mixed-citation></ref>
<ref id="c5"><label>[5]</label><mixed-citation publication-type="other"><source>The variant call format (vcf) version 4.3 specification</source>. <ext-link ext-link-type="uri" xlink:href="https://github.com/samtools/hts-specs/raw/master/VCFv4.3.pdf">https://github.com/samtools/hts-specs/raw/master/VCFv4.3.pdf</ext-link>, Oct <year>2018</year>.</mixed-citation></ref>
<ref id="c6"><label>[6]</label><mixed-citation publication-type="journal"><string-name><surname>Adrian</surname> <given-names>Tan</given-names></string-name>, <string-name><surname>Gon&#x00E7;alo</surname> <given-names>R. Abecasis</given-names></string-name>, and <string-name><surname>Hyun Min</surname> <given-names>Kang</given-names></string-name>. <article-title>Unified representation of genetic variants</article-title>. <source>Bioinformatics</source>, <volume>31</volume>(<issue>13</issue>):<fpage>2202</fpage>&#x2013;<lpage>2204</lpage>, <year>2015</year>.</mixed-citation></ref>
<ref id="c7"><label>[7]</label><mixed-citation publication-type="journal"><string-name><surname>Adrian</surname> <given-names>Tan</given-names></string-name>. <source>Vt normalize</source>. <ext-link ext-link-type="uri" xlink:href="https://genome.sph.umich.edu/wiki/Vt#Normalization">https://genome.sph.umich.edu/wiki/Vt#Normalization</ext-link>, Mar <year>2013</year>.</mixed-citation></ref>
<ref id="c8"><label>[8]</label><mixed-citation publication-type="other"><collab>Bcftools norm</collab>. <ext-link ext-link-type="uri" xlink:href="https://samtools.github.io/bcftools/bcftools.html#norm">https://samtools.github.io/bcftools/bcftools.html#norm</ext-link>, <year>2018</year>.</mixed-citation></ref>
<ref id="c9"><label>[9]</label><mixed-citation publication-type="journal"><string-name><surname>Athel</surname> <given-names>Cornish-Bowden</given-names></string-name>. <article-title>Nomenclature for incompletely specified bases in nucleic acid sequences: rcommendations 1984</article-title>. <source>Nucleic Acids Research</source>, <volume>13</volume>(<issue>9</issue>):<fpage>3021</fpage>&#x2013;<lpage>3030</lpage>, <year>1985</year>.</mixed-citation></ref>
<ref id="c10"><label>[10]</label><mixed-citation publication-type="other"><collab>Austin Appleby. Murmurhash3</collab>. <ext-link ext-link-type="uri" xlink:href="https://github.com/aappleby/smhasher/wiki/MurmurHash3">https://github.com/aappleby/smhasher/wiki/MurmurHash3</ext-link>, Mar <year>2012</year>.</mixed-citation></ref>
</ref-list>
</back>
</article>

# ScienceBeam Example Data

Selected example documents. Most of the data is coming from the train subset of the
[bioRxiv 10k dataset](https://zenodo.org/record/3873702).
